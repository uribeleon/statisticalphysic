\select@language {english}
\contentsline {section}{Introduction}{1}{Doc-Start}
\contentsline {section}{\numberline {1}Monte Carlo Cun\'atico}{2}{section.1}
\contentsline {subsection}{\numberline {1.1} Part\'icula en un potencial arm\'onico con $T\rightarrow 0$}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Part\'icula en un potencial arm\'onico con $T$ finito}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Matrix Squaring y Path-Integral Quantum Monte Carlo}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Matrix Squaring}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Integrales de trayectoria con Monte Carlo}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Part\'icula en un potencial anarm\'onico }{6}{subsection.2.3}
\contentsline {section}{\numberline {3}2D Ising Model}{7}{section.3}
\contentsline {section}{\numberline {4}Conclusiones}{8}{section.4}
\contentsline {section}{\numberline {5}Observaciones}{9}{section.5}
\contentsline {section}{References}{9}{section*.20}
\contentsfinish 
