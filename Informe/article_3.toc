\select@language {english}
\contentsline {section}{Introduction}{1}{Doc-Start}
\contentsline {section}{\numberline {1}Quantum Monte Carlo}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Particle in an harmonic potential with $T\rightarrow 0$}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Particle in an harmonic potential with finite $T$}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Matrix Squaring and Path-Integral Quantum Monte Carlo}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Matrix Squaring}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Subsection}{4}{subsection.2.2}
\contentsline {paragraph}{Paragraph}{4}{paragraph*.7}
\contentsline {paragraph}{Paragraph}{4}{paragraph*.8}
\contentsline {subsection}{\numberline {2.3}Subsection}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Results and Discussion}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Subsection}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Subsubsection}{4}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Subsubsection}{4}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Subsubsection}{4}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Subsection}{4}{subsection.3.2}
\contentsline {section}{Acknowledgments}{4}{section*.10}
\contentsline {section}{References}{4}{section*.11}
\contentsfinish 
