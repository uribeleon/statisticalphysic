Advanced Statistical Physics
==================================
by: **Cesar Alfredo Uribe Leon**

Homeworks
---------------

### Homework 1

_Notebook:_ [Metropolis algorithm with harmonic potential](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/statisticalphysic/raw/7795da3f48e9340b669d0df1af9465c6e63ae438/Tarea1/Tarea1-AStatisticalPhysics.ipynb)

### Homework 2
_Notebook:_ [Matrix squaring,Trotter formula and path-integral quantum Monte Carlo](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/statisticalphysic/raw/dd8ac92773ea4dfec36854259982d4d91b7d5047/Tarea2/Tarea2-AStatisticalPhysics.ipynb)

### Homework 3
_Notebook:_ [2D Ising Model](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/statisticalphysic/raw/dd8ac92773ea4dfec36854259982d4d91b7d5047/Tarea3/Tarea3-AStatisticalPhysics.ipynb)