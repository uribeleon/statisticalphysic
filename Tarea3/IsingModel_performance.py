from MyConfig import *

L = 128
N = L*L
T = 1.0
Beta= 1.0/T
S = Read_IniConfig(n=N) #Reading initial configuration (random configuration in this case)
S0 = S.copy()
nsteps = 2000
energy = GetEnergy_Ising(S,L,Beta,nsteps,msg=True)
enlb = str(energy)[:7]
PlotIsing(S0,S,L,T,savefig=True,namefig="Local_L"+str(L)+"_T"+str(T)+"_step"+str(nsteps)+"_En" + enlb + "_Configs.pdf")
