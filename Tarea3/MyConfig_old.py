#only for notebooks
#%pylab inline 

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# BASIC PACKAGES
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import scipy as sp
import os,sys
from Colors import *
from mpl_toolkits.axes_grid1 import AxesGrid


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GENERAL PARAMETERS OF PLOT
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mpl.rcParams['xtick.labelsize'] = 17
mpl.rcParams['ytick.labelsize'] = 17
mpl.rcParams['font.family'] = 'FreeSerif'
mpl.rcParams['xtick.minor.size'] = 3
mpl.rcParams['font.size'] = 17.0

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# ABREVIATIONS
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PI = np.pi


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# FUNCTIONS 
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def Energy(S,N,nbr):
    E = 0.0
    for k in xrange(N):
        E -= S[k] * np.sum(S[nbr[k]])
    return 0.5*E

def GetEnergy_Ising(S,L,Beta,nsteps,msg=True):
    N = L * L
    nsteps *= N
    nbr = np.array([ [(i//L)*L + (i + 1)%L, (i + L)%N, (i//L)*L + (i - 1)%L, (i - L)%N ] for i in xrange(N)])
    En = Energy(S,N,nbr)
    E = []
    for step in xrange(nsteps):
        k = np.random.randint(0,N-1)
        deltaE = 2.0*S[k]*np.sum(S[nbr[k]])
        if np.random.uniform(0.0,1.0) < np.exp(-Beta*deltaE):
            S[k] *= -1
            En += deltaE
        E.append(En)
        sys.stdout.write("Progress: %.2f %%\r"%((100.0*step)/nsteps))
    if msg:
        print "Mean energy per spin: %.4f"%(np.sum(E)/(1.0*N*len(E)))
    return np.sum(E)/(1.0*N*len(E))

def Read_IniConfig(filename='',n=0):
    if os.path.isfile(filename):
        S = np.loadtxt(filename)
        print "Starting from file %s"%filename
    else:
        S = np.array([np.random.choice([-1,1]) for k in xrange(n)])
        print 'Starting from a random configuration with N=%d'%n
    return S

def PlotIsing(S0,S,L,T,figS = (20,20),savefig=False,namefig=" "):
    conf = list()
    conf.append(np.reshape(S0,(L,L)))
    conf.append(np.reshape(S,(L,L)))
    
    fig = plt.figure(1,figsize=(20,20))
    #fig.subplots_adjust(left=0.05, right=0.95)
    grid = AxesGrid(fig, 122,  # similar to subplot(142)
                    nrows_ncols=(1, 2),
                    axes_pad=0.5,
                    share_all=True,
                    label_mode="all",
                    cbar_location="right",
                    cbar_mode="single",
                    cbar_pad=0.2,
                    aspect = True
                    )
    Sconf = ["Initial","Final"]
    for i in xrange(2):
        im=grid[i].imshow(conf[i],extent=[0,L,L,0],interpolation='nearest')
        grid[i].set_title('Local_L' + str(L) + '_T' + str(T) + ' (%s)'%Sconf[i])
    plt.set_cmap('jet')
    grid.cbar_axes[0].colorbar(im)
    
    if savefig:
        plt.savefig(namefig,bbox_inches='tight')