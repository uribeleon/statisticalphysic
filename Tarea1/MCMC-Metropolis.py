from MyModules import *

def fp(x):
	return np.exp(-0.5*x**2)

Ntrial = 1000000
dx = 2.0
x = np.zeros(Ntrial)
xx=0.0
xlist = list()
xfunc = np.linspace(-dx,dx,100)
Naccepted = 0.0

"""
for i in xrange(Ntrial):
	xnew = xx + np.random.uniform(-dx,dx)
	if ( np.random.rand() < fp(xnew)/fp(xx)):
		x = xnew
	xlist.append(x)
"""

for i in xrange(1,Ntrial):
	xnew = x[i-1] + np.random.uniform(-dx,dx)
	Gamma = Phi0(xnew)**2/Phi0(x[i-1])**2
	if ( np.random.rand() < Gamma):
		x[i] = xnew
		Naccepted+=1
	else:
		x[i] = x[i-1]

bins=np.linspace(-dx,dx,40)
#bins = np.arange(-dx,dx+0.1,0.1)
N,Bins,patches=plt.hist(x,bins,normed=True,align="mid",color='g',alpha=0.5)
plt.plot(xfunc,Phi0(xfunc)**2,'r--',lw=2.5)
plt.xlabel()
#plt.plot(xfunc,fp(xfunc),'r--',lw=2.5)
plt.savefig("Grafico1.pdf")
print "Ntrial: %d   Naccepted: %d   Percentage: %f"%(Ntrial,Naccepted,(100.0*Naccepted)/Ntrial)


